<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PagesDescription extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('page_description', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('page_id', false, true);
            $table->integer('language_id', false, true);

            $table->string('title')->nullable();
            $table->text('notice')->nullable();
            $table->longText('content')->nullable();

            $table->string('seo_title')->nullable();
            $table->text('seo_keywords')->nullable();
            $table->text('seo_description')->nullable();
            $table->string('h1')->nullable();
            $table->text('tags')->nullable();
            
            $table->timestamps();

            $table->index('page_id');
            $table->index('language_id');

            $table->foreign('page_id', 'page_description_page_id')->references('id')->on('page')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('language_id', 'page_description_language_id')->references('id')->on('language')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('page_description');
    }
}
