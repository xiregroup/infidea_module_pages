<?php

namespace Modules\Pages\Models;

use App\Models\Attributes\Traits\Attribute;
use App\Models\MediaLibrary\Traits\Library;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Page extends Model
{
    use SoftDeletes, Library, Attribute;
    
    protected $module = 'pages';
    protected $table = 'page';
    protected $primaryKey = 'id';
    protected $attributes = [
        'active' => 1
    ];

    protected $dates = ['deleted_at', 'updated_at', 'created_at'];

    public function getChilds($id, &$data = array())
    {
        $results = self::where('parent_id', $id)->get(['id']);
        foreach ($results as $result) {
            $data[] = $result->id;
            self::getChilds($result->id, $data);
        }

        return $data;
    }

    public function getUrl()
    {
        return route('custom', ['module' => $this->module, 'id' => $this->id]);
    }

    public function getParentPageName($language)
    {
        if ($this->parent_id) {
            $result = self::join('page_description', 'page_description.page_id', '=', 'page.id')
                ->where('page_description.language_id', $language)
                ->where('page.id', $this->parent_id)
                ->first(['title']);

            if ($result) {
                return $result->title;
            }
        }

        return false;
    }

    public function hasChilds($currentId = false)
    {
        $result = self::where('parent_id', $this->id);
        if ($currentId){
            $result->where('page.id', '!=', $currentId);
        }

        return $result->count() > 0;
    }

    public static function getTree($language, $parent, $currentId=false)
    {
        $result = self::join('page_description', 'page.id', '=', 'page_description.page_id')
            ->where(['page_description.language_id' => $language]);

        if ($parent) {
            $result->where('page.parent_id', $parent);
        } else {
            $result->whereNull('page.parent_id');
        }

        if ($currentId){
            $result->where('page.id', '!=', $currentId);
        }

        return $result->get(['page.id', 'page_description.title as text', 'page.parent_id']);
    }

    public function getParentsUrl(&$url = [])
    {
        if ($this->parent_id){
            $result = self::where('id', $this->parent_id)->first(['url', 'parent_id']);
            if ($result){
                if ($result->parent_id){
                    self::getParentsUrl($url);
                    $url[] = $result->url;
                }
            }
        }

        return $url;
    }

    public static function getItems($language, $start = 0, $length = 10, $orderBy = array('column' => 'id', 'dir' => 'asc'), $archive = false)
    {
        $result =  self::join('page_description', 'page.id', '=', 'page_description.page_id')
            ->where(['page_description.language_id' => $language])->skip($start)->take($length)
            ->orderBy($orderBy['column'], $orderBy['dir']);
        if ($archive){
            $result->onlyTrashed();
        }

        return $result->get(['page.id', 'page.active', 'page.created_at', 'page.position', 'page.updated_at', 'page_description.title', 'page.parent_id']);
    }

    public function description()
    {
        $this->hasMany('Modules\Pages\Models\PageDescription', 'page_id', 'id');
    }


}