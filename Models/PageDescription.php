<?php

namespace Modules\Pages\Models;

use Illuminate\Database\Eloquent\Model;

class PageDescription extends Model
{
    protected $table = 'page_description';

    protected $primaryKey = 'id';

    protected $attributes = [
        'id' => null,
        'page_id' => null,
        'language_id' => null,
        'title' => null,
        'notice' => null,
        'content' => null,
        'seo_title' => null,
        'seo_keywords' => null,
        'seo_description' => null,
        'h1' => null,
        'tags' => null
    ];

    public function page()
    {
        return self::hasOne('Modules\Pages\Models\Page', 'id', 'page_id')->withTrashed();
    }

    public function initSeo()
    {
        if ($this->seo_title){
            app('seo')->setTitle($this->seo_title);
        }
        if ($this->seo_description){
            app('seo')->setDescription($this->seo_description);
        }
        if ($this->seo_keywords){
            app('seo')->setKeywords($this->seo_keywords);
        }
    }

    public function hasActive()
    {
        return $this->page->active  == 1 && !$this->page->trashed();
    }

    
    public static function findById($id, $language)
    {
        return self::where(['page_id' => $id, 'language_id' => $language])->first();
    }
}