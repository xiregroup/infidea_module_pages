<?php

Route::group(['prefix'=>'admin', 'namespace'=>'ControlPanel'], function () {
    Route::get('pages', ['as' => 'module.pages', 'middleware' => 'admin', 'uses' => 'IndexController@index']);
    Route::get('pages/loadTree', ['as' => 'module.pages.load_tree', 'middleware' => 'admin', 'uses' => 'IndexController@loadTree']);
    Route::get('pages/modal', ['as' => 'module.pages.modal', 'middleware' => 'admin', 'uses' => 'IndexController@getModal']);
    Route::match( ['GET', 'POST'], 'pages/add/', ['as' => 'module.pages.add', 'middleware' => 'admin', 'uses' => 'IndexController@add']);
    Route::match( ['GET', 'POST'], 'pages/edit/{id?}', ['as' => 'module.pages.edit', 'middleware' => 'admin', 'uses' => 'IndexController@add']);
    Route::delete('pages/delete/{id}', ['as' => 'module.pages.delete', 'middleware' => 'admin', 'uses' => 'IndexController@deletePage']);
    Route::put('pages/update/{id}', ['as' => 'module.pages.update', 'middleware' => 'admin', 'uses' => 'IndexController@updatePage']);
    Route::put('pages/recovery/{id}', ['as' => 'module.pages.recovery', 'middleware' => 'admin', 'uses' => 'IndexController@recoveryPage']);
});