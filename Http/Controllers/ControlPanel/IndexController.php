<?php

namespace Modules\Pages\Http\Controllers\ControlPanel;

use App\Core\ViewHelper;
use App\Helpers\StringHelpers;
use App\Http\Controllers\Controller;
use App\Models\Language;
use App\Models\Route;
use Illuminate\Http\Request;
use Modules\Pages\Models\Page;
use Modules\Pages\Models\PageDescription;

class IndexController extends Controller
{
    /**
     * IndexController constructor.
     */
    public function __construct()
    {
        app('breadcrumbs')->add(trans('pages::app.name'), route('module.pages'));
    }

    /**
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updatePage($id, Request $request)
    {
        $page = Page::withTrashed()->find($id);
        if ($page) {
            $page->{$request->input('field')} = $request->input('value');
            $page->save();

            return response()->json(['success' => true]);
        }

        return response()->json(['failure' => true], 404);
    }

    /**
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function recoveryPage($id, Request $request)
    {
        $page = Page::withTrashed()->find($id);
        if ($page) {
            $page->restore();

            return response()->json(['success' => true]);
        }

        return response()->json(['failure' => true], 404);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\JsonResponse|\Illuminate\View\View
     */
    public function index(Request $request)
    {

        if ($request->ajax()) {
            $archive = $request->input('archive', 0);
            if (!$archive) {
                $total = Page::count();
            } else {
                $total = Page::onlyTrashed()->count();
            }
            $columns = $request->input('columns');
            $orderBy = $request->input('order');
            $orderBy = ['column' => $columns[$orderBy[0]['column']]['name'], 'dir' => $orderBy[0]['dir']];

            $results = [
                'data' => [],
                'draw' => $request->input('draw', 1),
                'recordsTotal' => $total,
                'recordsFiltered' => $total,
            ];

            foreach (Page::getItems(app()->language->getDefault(), (int)$request->input('start', 0), (int)$request->input('length', 0), $orderBy, $request->input('archive', 0)) as $page) {

                $actions = '<div class="btn-group" role="group" aria-label="...">
 <a href="' . route('module.pages.edit', ['id' => $page->id]) . '" type="button"
  data-toggle="tooltip" data-placement="left" title="Редактировать"
                   class="btn btn-default"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>';


                if ($page->hasChilds() == 0 && !$archive) {
                    $actions .= '<button type="button"
                            data-url = "' . route('module.pages.delete', ['id' => $page->id]) . '"
                            onclick = "deletePage(this)" data-id = "' . $page->id . '"
                            data-toggle="tooltip" data-placement="left" title="Удалить"
                            data-message="Вы уверены что хотите удалить эту страницу?" data-btnyes="Да"
                            data-btnno="Нет" class="btn btn-default" ><i class="fa fa-trash"
                                                                        aria-hidden="true" ></i></button >';
                }
                if ($archive) {
                    $actions .= '<button type="button"
                            data-url = "' . route('module.pages.recovery', ['id' => $page->id]) . '"
                            onclick = "recoveryPage(this)" data-id = "' . $page->id . '"
                            data-toggle="tooltip" data-placement="left" title="Восстановить"
                            data-message="Вы уверены что хотите восстановить эту страницу?" data-btnyes="Да"
                            data-btnno="Нет" class="btn btn-default" ><i class="fa fa-reply"
                                                                        aria-hidden="true" ></i></button >';
                }
                $actions .= '</div>';

                $results['data'][] = array(
                    $page->id,
                    $page->title,
                    '<a href="javascript:;" class="editable"
               data-url="' . route('module.pages.update', ['id' => $page->id, 'field' => 'position']) . '"
               data-type="text" data-name="position" data-pk="1" data-placement="right" data-placeholder="0"
               data-original-title="Позиция">' . $page->position . '</a>',
                    '<a href="javascript:;" class="editable"
               data-url="' . route('module.pages.update', ['id' => $page->id, 'field' => 'active']) . '"
               data-type="select" data-name="active"
               data-source="' . htmlspecialchars(json_encode([['value' => 1, 'text' => 'Опубликовано'], ['value' => 0, 'text' => 'Не опубликовано']]), ENT_COMPAT, 'utf-8') . '"
               data-value="' . $page->active . '" data-pk="1" data-placement="right"
               data-original-title="Статус">' . ($page->active ? 'Опубликовано' : 'Не опубликовано') . '</a>',
                    route('slug', ['module' => 'pages', 'id' => $page->id]),
                    $page->updated_at->format('d.m.Y H:i'),
                    $actions
                );
            }


            return response()->json($results);
        }
        app()->viewHelper->addScript('/assets/global/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js');
        app()->viewHelper->addScript('/assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js');
        app()->viewHelper->addScript('/assets/global/plugins/bootbox/bootbox.min.js');
        app()->viewHelper->addStyle('/assets/global/plugins/bootstrap-toastr/toastr.min.css');
        app()->viewHelper->addScript('/assets/global/plugins/bootstrap-toastr/toastr.min.js');
        app()->viewHelper->addStyle(['/assets/global/plugins/datatables/datatables.min.css',
            '/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css']);
        app('viewHelper')->addScript(['/assets/global/scripts/datatable.js',
            '/assets/global/plugins/datatables/datatables.min.js',
            '/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js']);


        app()->viewHelper->addScript(route('assets', ['module' => 'pages', 'file' => "jquery.treetable.js"]));
        app()->viewHelper->addScript(route('assets', ['module' => 'pages', 'file' => "pages.js"]));

        return view('pages::admin.index');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function loadTree(Request $request)
    {
        $pages = Page::getTree(app()->language->getDefault(), (int)$request->input('parent', null), (int)$request->input('currentId', false));

        if ((int)$request->input('parent', null) == 0) {
            $data = [['id' => '0', 'children' => false, 'text' => "Основной раздел"]];
        } else {
            $data = [];
        }
        foreach ($pages as $page) {
            $data[] = array_merge(['children' => $page->hasChilds((int)$request->input('currentId', false))], $page->toArray());
        }

        return response()->json($data);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function deletePage($id)
    {
        // @todo: use transltor
        $title = 'Удаление страницы';

        $page = Page::find($id);
        if ($page) {
            if ($page->hasChilds()) {
                // @todo: use transltor
                return response()->json(['failure' => true, 'title' => $title, 'message' => 'У страницы есть подразделы']);
            } else {
                //@todo: Удалять картинки
                $page->delete();
                // @todo: use transltor
                return response()->json(['success' => true, 'title' => $title, 'message' => 'Страница успешно удалена']);
            }
        }

        return response()->json(['failure' => true, 'title' => $title, 'message' => 'Страница не найдена']);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getModal()
    {
        return view('pages::admin.modal');
    }

    /**
     * @param Request $request
     * @param null $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function add(Request $request, $id = null)
    {
        $lang = $request->input('lang', app()->language->getDefault());
        $pageDescription = null;
        if ($id !== null) {
            app('breadcrumbs')->add(trans('pages::app.edit_page'), route('module.pages.edit'));
            $pageDescription = PageDescription::where(['language_id' => $lang, 'page_id' => $id])->first();
        } else {
            app('breadcrumbs')->add(trans('pages::app.add_page'), route('module.pages.add'));
        }
        if (!$pageDescription) {
            $pageDescription = new PageDescription();
        }

        $parentName = 'Основной раздел';
        $page = new Page();
        if ($id !== null) {
            $page = Page::withTrashed()->find($id);
            if (!$page) {
                $page = new Page();
            } else {
                if (($temporaryParentName = $page->getParentPageName($lang)) != false
                    || ($temporaryParentName = $page->getParentPageName(app()->language->getDefault())) != false
                ) {
                    $parentName = $temporaryParentName;
                }
            }
        }
        $errors = [];

        if ($request->isMethod('POST')) {

            $data = $request->only(['title', 'content', 'h1', 'seo_title', 'seo_keywords', 'seo_description', 'tags']);
            $dataPage = $request->only(['url', 'active', 'parent_id']);
            foreach ($data as $key => $value) {
                $pageDescription->{$key} = $value;
            }
            $url = $dataPage['url'];
            $active = in_array((int)$dataPage['active'], [0, 1]) ? (int)$dataPage['active'] : 1;
            $parentId = (int)$dataPage['parent_id'];
            if (empty($url)) {
                $url = StringHelpers::getTranslit($pageDescription->title);
            }
            $url = trim($url);
            $page->url = $url;
            $page->active = $active;

            if (!is_null($parentId) && $parentId != $page->parentId && $page->id !== null) {
                $childs = $page->getChilds($page->id);
                if (in_array($parentId, $childs)) {
                    $errors['parent_id'] = 'Нельзя выбрать данный родительский раздел';
                }
            } else {
                if ($parentId === 0) {
                    $page->parent_id = null;
                } else {
                    $page->parent_id = $parentId;
                }
            }

            if (count($errors) == 0) {

                $page->save();

                $pageDescription->page_id = $page->id;
                $pageDescription->language_id = $lang;

                $pageDescription->save();
                $page->addFiles($lang);
                if (app()->language->getDefault() == $lang) {
                    $prefixUrl = '';
                    if ($page->parent_id) {
                        if (($route = Route::where(['module' => 'pages', 'item_id' => $page->parent_id,
                                'action' => '\\Modules\Pages\Http\Controllers\IndexController@page'])->first(['id', 'url'])) != false
                        ) {
                            $prefixUrl = $route->url . '/';
                        }
                    }

                    $changed = false;
                    if (($route = Route::where(['module' => 'pages', 'item_id' => $page->id,
                            'action' => '\\Modules\Pages\Http\Controllers\IndexController@page'])->first(['id', 'url'])) != false
                    ) {
                        if ($route->url != $prefixUrl . $url) {
                            $changed = true;
                            $route->url = $prefixUrl . $url;
                        }
                    } else {
                        $changed = true;
                        $route = new Route();
                        $route->module = 'pages';
                        $route->item_id = $page->id;
                        $route->action = '\\Modules\Pages\Http\Controllers\IndexController@page';
                        $route->url = $prefixUrl . $url;
                    }
                    $route->save();
                    if ($changed) {
                        app()->events->fire('modules.pages.update_route', ['url' => $route->url . '/', 'id' => $page->id]);
                    }
                }

                return redirect()->route('module.pages');
            }
        }

        app()->viewHelper->addStyle('/assets/global/plugins/jstree/dist/themes/default/style.min.css');
        app()->viewHelper->addStyle('/assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css');
        app()->viewHelper->addScript('/assets/global/plugins/jstree/dist/jstree.min.js');
        app()->viewHelper->addScript('/assets/global/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js');
        app()->viewHelper->addScript('/assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js');
        app()->viewHelper->addScript(route('assets', ['module' => 'pages', 'file' => "jquery.treetable.js"]));
        app()->viewHelper->addScript(route('assets', ['module' => 'pages', 'file' => "pages.js"]));

        return view('pages::admin.add', ['page' => $pageDescription, 'row' => $page, 'errors' => $errors,
            'parentName' => $parentName, 'currentLanguage' => $lang]);
    }

}