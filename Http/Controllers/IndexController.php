<?php

namespace Modules\Pages\Http\Controllers;

use App\Http\Controllers\Controller;
use Modules\Pages\Models\PageDescription;
use View;
use Cache;

class IndexController extends Controller
{
    public function page($id)
    {
       $cacheKey = app('modules')->getCurrentModule()->getLowerName().'_record_'.$id;
       if (Cache::has($cacheKey)){
           return Cache::get($cacheKey);
       }
       $page = PageDescription::findById($id, app()->language->getDefault());
       if ($page)
       {
           // if page not active or trashed
           if (!$page->hasActive()){
               return abort(404);
           }
           // set SEO params
           $page->initSeo();

           $view = 'pages::page';
           /**
            * if exists other view for this page
            * @example views/page_1.blade.php
            */
           if (View::exists('pages::page_'.$page->page_id)){
               $view = 'pages::page_'.$page->page_id;
           }

           $view = view($view, ['page' => $page])->render();
           Cache::forever($cacheKey, $view);

           return $view;
       }

       return abort(404);
    }
}