$('table.pages').treetable({
    expandable: true,
    'stringExpand': 'glyphicon glyphicon-chevron-right',
    'stringCollapse': 'glyphicon glyphicon-chevron-down'
});


$('.wysihtml5').wysihtml5({
    "stylesheets": ["/assets/global/plugins/bootstrap-wysihtml5/wysiwyg-color.css"]
});

var modal = null;
function selectParentPage() {
    var id = $("#pagesTree").jstree('get_selected');
    if (id.length > 0) {
        id = id[0];
        var text = $("#pagesTree").jstree('get_text', id);
        $('#pagesForm input[name=parent_id]').val(id);
        $('#pagesForm input[name=parentName]').val(text);
    }

    modal.modal('hide');
}

function deletePage(el) {
    bootbox.confirm({
        message: $(el).attr('data-message'),
        buttons: {
            confirm: {
                label: $(el).attr('data-btnyes'),
                className: 'btn-success'
            },
            cancel: {
                label: $(el).attr('data-btnno'),
                className: 'btn-danger'
            }
        },
        callback: function (result) {
            if (result) {
                $.ajax({
                    url: $(el).attr('data-url'),
                    dataType: 'json',
                    type: 'delete',
                    data: {id: $(el).attr('data-id')},
                    success: function (response) {
                        if (response.success) {
                            toastr['success'](response.message, response.title)
                            $(el).parents('tr').remove();
                        }
                        else {
                            toastr['warning'](response.message, response.title)
                        }
                    },
                    error: function () {
                        alert('Something wrong');
                    }
                });
            }
        }
    });
}
function recoveryPage(el) {
    bootbox.confirm({
        message: $(el).attr('data-message'),
        buttons: {
            confirm: {
                label: $(el).attr('data-btnyes'),
                className: 'btn-success'
            },
            cancel: {
                label: $(el).attr('data-btnno'),
                className: 'btn-danger'
            }
        },
        callback: function (result) {
            if (result) {
                $.ajax({
                    url: $(el).attr('data-url'),
                    dataType: 'json',
                    type: 'put',
                    data: {id: $(el).attr('data-id')},
                    success: function (response) {
                        if (response.success) {
                            toastr['success'](response.message, response.title)
                            $(el).parents('tr').remove();
                        }
                        else {
                            toastr['warning'](response.message, response.title)
                        }
                    },
                    error: function () {
                        alert('Something wrong');
                    }
                });
            }
        }
    });
}
var gridPagesData = {
    archive: 0
};
function getArchive(el)
{
    el = $(el);
    // @todo:  get from data element for multi language
    if (el.hasClass('archive')){
        el.removeClass('archive');
        el.text('Архив');
        gridPagesData.archive = 0;
    }
    else {
        el.addClass('archive');
        el.text('Актуальные');
        gridPagesData.archive = 1;
    }
    gridPages.getDataTable().ajax.reload();
}

if ($('#modulePagesGrid').length>0) {
    var gridPages = new Datatable();

    gridPages.init({
        src: $("#modulePagesGrid"),

        onSuccess: function (grid, response) {
           // if ($('.editable').length>0) {

            //}
        },
        onError: function (grid) {
            // execute some code on network or other general error
        },
        onDataLoad: function (grid) {
            // execute some code on ajax data load
            $('.editable').editable({
                ajaxOptions: {
                    type: "PUT"
                }
            });
        },
        loadingMessage: 'Loading...',
        dataTable: {
            "language": {
                "url" : "/assets/global/plugins/datatables/i18n/"+$('#modulePagesGrid').attr('data-lang')+'.js'
            },
            "bProcessing": true,
            "bServerSide": true,
            "bDeferRender": true,
            loadingMessage: 'Loading...',
            "columns": [
                {"name": "page.id", "orderable": true},
                {"name": "page_description.title", "orderable": true},
                {"name": "page.position", "orderable": true},
                {"name": "page.active", "orderable": true},
                {"name": "link", "orderable": false},
                {"name": "page.updated_at", "orderable": true},
                {"name": "actions", "orderable": false},
            ],
            "bStateSave": true,
            // read the custom filters from saved state and populate the filter inputs
            "fnStateLoadParams": function (oSettings, oData) {
                //Load custom filters
                $("#datatable_ajax tr.filter .form-control").each(function () {
                    var element = $(this);
                    if (oData[element.attr('name')]) {
                        element.val(oData[element.attr('name')]);
                    }
                });

                return true;
            },

            "lengthMenu": [
                [10, 20, 50, 100, 150],
                [10, 20, 50, 100, 150]
            ],
            "pageLength": 10, // default record count per page
            "ajax": {
                "data" : function(d){  return $.extend(d,gridPagesData); },
                "type": 'GET',
                "url": "/admin/pages", // ajax source
            },
            "order": [
                [0, "desc"]
            ]// set first column as a default sort by asc
        }
    });
}

function pageSelectParent() {
    $.ajax({
        url: '/admin/pages/modal',
        success: function (response) {
            modal = $(response).modal();
            modal.on('shown.bs.modal', function () {
                $("#pagesTree").jstree({
                    "core": {
                        "themes": {
                            "responsive": false
                        },
                        // so that create works
                        "check_callback": true,
                        'data': {
                            'url': function (node) {
                                return '/admin/pages/loadTree?currentId=' + $('#currentId').val();
                            },
                            'data': function (node) {
                                return {'parent': node.id};
                            }
                        }
                    },
                    "types": {
                        "default": {
                            "icon": "fa fa-folder icon-state-warning icon-lg"
                        }
                    }

                });
            });

        }
    });
}