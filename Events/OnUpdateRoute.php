<?php

namespace Modules\Pages\Events;

use App\Models\Route;
use Modules\Pages\Models\Page;

class OnUpdateRoute
{
    public function handle($url, $id)
    {
        $this->recursiveUpdate($id, $url);
    }

    protected function recursiveUpdate($parent_id, $prefixUrl)
    {
        $pages = Page::where('parent_id', $parent_id)->get(['id', 'url']);
       
        foreach ($pages as $page)
        {
            if (($route = Route::where(['module' => 'pages', 'item_id' => $page->id,
                    'action' => '\\Modules\Pages\Http\Controllers\IndexController@page'])->first(['id', 'url'])) != false
            ) {

                $route->url = trim($prefixUrl.$page->url, '/');
                $route->save();

                $this->recursiveUpdate($page->id, $route->url.'/');
            }
        }
    }

}
