<?php

namespace Modules\Pages\Providers;

use Illuminate\Support\ServiceProvider;
use Modules\Pages\Models\Page;
use Modules\Pages\Models\PageDescription;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        /**
         * Before delete record
         */
        Page::deleting(function (Page $page) {
            $page->attachments()->delete();
        });
        /**
         * On update Record description update date main record
         */
        PageDescription::updating(function (PageDescription $description) {
            $page = $description->page;
            $page->updated_at = new \DateTime();
            $page->save();
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {

    }
}
