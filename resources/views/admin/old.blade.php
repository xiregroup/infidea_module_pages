@foreach ($pages as $page)
    <tr data-tt-id="{{$page->id}}" {!! $page->parent_id  ? 'data-tt-parent-id="'.$page->parent_id.'"' : '' !!}>
        <td>{!! $page->id !!}</td>
        <td>{!! $page->title !!}</td>
        <td><a href="javascript:;" class="editable"
               data-url="{{ route('module.pages.update', ['id' => $page->id, 'field' => 'position']) }}"
               data-type="text" data-name="position" data-pk="1" data-placement="right" data-placeholder="0"
               data-original-title="Позиция">{!! $page->position !!}</a></td>
        <td><a href="javascript:;" class="editable"
               data-url="{{ route('module.pages.update', ['id' => $page->id, 'field' => 'active']) }}"
               data-type="select" data-name="active"
               data-source="{{ json_encode([['value' => 1, 'text' =>'Опубликовано'],['value' => 0, 'text' =>'Не опубликовано']]) }}"
               data-value="{{$page->active}}" data-pk="1" data-placement="right"
               data-original-title="Статус">{!! $page->active ? 'Опубликовано' : 'Не опубликовано' !!}</a>
        </td>
        <td>{!! route('slug', ['module' => 'pages', 'id' => $page->id]) !!}</td>
        <td>
            <div class="btn-group" role="group" aria-label="...">

                <a href="{!! route('module.pages.edit', ['id' => $page->id]) !!}" type="button"
                   class="btn btn-default"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>

                @if ($page->hasChilds() == 0)
                    <button type="button"
                            data-url="{!! route('module.pages.delete', ['id' => $page->id]) !!}"
                            onclick="deletePage(this)" data-id="{{$page->id}}"
                            data-message="Вы уверены что хотите удалить эту страницу?" data-btnyes="Да"
                            data-btnno="Нет" class="btn btn-default"><i class="fa fa-trash"
                                                                        aria-hidden="true"></i></button>
                @endif
            </div>
        </td>
    </tr>
@endforeach