@extends('admin.layout.main')
@section('title_page')
    {!! trans('pages::app.name') !!}
@endsection
@section('content')

    <form method="post" id="pagesForm">
        <input type="hidden" name="currentId" value="{{$row->id}}" id="currentId"/>
        <div class="form-body">
            <div class="portlet light">
                <div class="portlet-title tabbable-line">
                    <ul class="nav nav-tabs pull-left" role="tablist">
                        <li role="presentation" class="active"><a href="#contentTab" aria-controls="home" role="tab"
                                                                  data-toggle="tab">Контент</a></li>
                        <li role="presentation"><a href="#images" aria-controls="home" role="tab" data-toggle="tab">Файлы</a></li>
                        <li role="presentation"><a href="#seoTab" aria-controls="home" role="tab" data-toggle="tab">SEO
                                Параметры</a></li>
                        <li role="presentation"><a href="#settingsTab" aria-controls="home" role="tab"
                                                   data-toggle="tab">Настройки</a></li>
                    </ul>

                    <div class="pull-right">

                        @if ($row->id)
                            <div class="btn-group" role="group" aria-label="...">
                                <button class="btn btn-default dropdown-toggle" type="button"
                                        id="dropdownMenuOtherLanguage" data-toggle="dropdown" aria-haspopup="true"
                                        aria-expanded="true">
                                    Другой язык
                                    <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu dropdown-menu-left" style="min-width: 140px;"
                                    aria-labelledby="dropdownMenuOtherLanguage">
                                    @foreach (app()->language->getList() as $language)
                                        @if ($language->id != $currentLanguage)
                                            <li>
                                                <a href="{!! route('module.pages.edit', ['id' => $row->id, 'lang' => $language->id]) !!}">{{$language->name}}</a>
                                            </li>
                                        @endif
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane fade" id="images">
                            @include('admin.media.list_files')
                        </div>
                        <div role="tabpanel" class="tab-pane fade active in" id="contentTab">
                            <div class="form-group">
                                <label for="form_control_1">Название</label>
                                <input type="text" name="title" class="form-control" value="{{$page->title}}"
                                       id="form_control_1"
                                       placeholder="Введите название страницы">

                            </div>
                            <div class="form-group col-md-4{!! key_exists('parent_id', $errors) ? ' has-error' : '' !!}">
                                <label for="form_control_1">Родительский раздел</label>
                                <div class="input-group">

                                    <input type="hidden" name="parent_id" value="{{$row->parent_id}}"/>

                                    <input type="text" name="parentName" class="form-control"
                                           value="{{$parentName}}"
                                           placeholder="Основной раздел" readonly>
 <span class="input-group-btn">
                                 <button type="button"
                                         class="btn blue"
                                         aria-expanded="false" onclick="pageSelectParent();"> Выбрать </button>

                            </span>


                                </div>
                            </div>
                            <div style="clear: both"></div>
                            <div class="form-group">
                                <label for="form_control_2">Контент страницы</label>
                        <textarea name="content" cols="10" rows="10" id="form_control_2"
                                  class="wysihtml5 form-control">{!! $page->content !!}</textarea>
                            </div>
                        </div>

                        <div role="tabpanel" class="tab-pane fade" id="seoTab">
                            <div class="form-group">
                                <label for="form_control_3">URL Страницы</label>
                                <input type="text" name="url" class="form-control" value="{{$row->url}}"
                                       id="form_control_3"
                                       placeholder="Введите название страницы">

                            </div>
                            <div class="form-group">
                                <label for="form_control_4">H1</label>
                                <input type="text" name="seo_title" class="form-control" value="{{$page->h1}}"
                                       id="form_control_4"
                                       placeholder="Введите название страницы">

                            </div>
                            <div class="form-group">
                                <label for="form_control_5">Заголовок</label>
                                <input type="text" name="seo_title" class="form-control" value="{{$page->seo_title}}"
                                       id="form_control_5"
                                       placeholder="Введите название страницы">

                            </div>
                            <div class="form-group">
                                <label for="form_control_6">Ключевые слова</label>
                        <textarea name="seo_keywords" class="form-control" id="form_control_6"
                                  placeholder="Ключеваые слова">{!! $page->seo_keywords !!}</textarea>

                            </div>
                            <div class="form-group">
                                <label for="form_control_7">Описание</label>
                        <textarea name="seo_description" class="form-control" id="form_control_7"
                                  placeholder="Описание страницы">{!! $page->seo_description !!}</textarea>

                            </div>
                            <div class="form-group">
                                <label for="form_control_8">Теги</label>
                                <input type="text" name="seo_title" class="form-control" value="{{$page->tags}}"
                                       id="form_control_8"
                                       placeholder="Введите название страницы">

                            </div>
                        </div>

                        <div role="tabpanel" class="tab-pane fade" id="settingsTab">
                            <div class="form-group col-md-2">
                                <label for="form_control_9">Опубликовано</label>
                                <select name="active" class="form-control" id="form_control_9">
                                    <option value="0"{!! $row->active == 0 ? ' selected="selected"' : '' !!}>Нет
                                    </option>
                                    <option value="1"{!! $row->active == 1 ? ' selected="selected"' : '' !!}>Да</option>
                                </select>

                            </div>
                            <div style="clear:both"></div>
                            <div class="form-group col-md-2">
                                <label for="form_control_12">Дата создания</label>
                                <input type="text" class="form-control"
                                       value="{{$row->created_at ? $row->created_at->format('d.m.Y H:i:s') : ''}}" readonly/>

                            </div>
                            <div style="clear:both"></div>
                            <div class="form-group col-md-2">
                                <label for="form_control_13">Дата изменения</label>
                                <input type="text" class="form-control"
                                       value="{{$row->updated_at ? $row->updated_at->format('d.m.Y H:i:s') : ''}}" readonly/>

                            </div>
                        </div>
                    </div>
                </div>

                <div style="clear: both;"></div>
                <button class="btn blue" type="submit">Сохранить</button>
                <a href="{!! route('module.pages') !!}" class="btn btn-default" type="submit">Не сохранять</a>
            </div>
        </div>
    </form>
@endsection