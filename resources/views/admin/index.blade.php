@extends('admin.layout.main')

@section('title_page')
    {{trans('pages::app.name')}}
@endsection

@section('content')

    <div class="portlet light">
        <div class="portlet-title">
            <div class="caption">
                Список страниц
            </div>
            <div class="actions">
                <a href="{!! route('module.pages.add') !!}" class="btn btn-circle red-sunglo ">
                    <i class="fa fa-plus"></i> Добавить </a>
                <a href="javascript:;" onclick="getArchive(this)" class="btn btn-circle btn-default ">Архив</a>
            </div>
        </div>
        <div class="portlet-body">
        <table class="pages table table-hover" data-lang="{!! App::getLocale() !!}" id="modulePagesGrid">
            <thead>
            <tr>
                <th>#</th>
                <th>Название</th>
                <th>Позиция</th>
                <th>Статус</th>
                <th>Ссылка</th>
                <th>Дата изменения</th>
                <th>Действия</th>
            </tr>
            </thead>
            <tbody>

            </tbody>

        </table>
        </div>
    </div>
@endsection