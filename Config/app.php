<?php

return [
    'name' => 'app.name',
    'namespace' => 'Pages',
    'prefix' => '',
    'middleware' => [

    ],
    'middlewaregroup' => [],
    'providers' => [
        \Modules\Pages\Providers\AppServiceProvider::class
    ],
    'commands' => [
        'install'=> \Modules\Pages\Console\InstallCommand::class
    ],
    'listen' => [
        'update_route' => [\Modules\Pages\Events\OnUpdateRoute::class, 'handle']
    ]
];